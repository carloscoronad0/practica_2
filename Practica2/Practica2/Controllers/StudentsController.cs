﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Practica2.Controllers
{
    [ApiController]
    [Route("/api/students")]
    public class StudentsController : ControllerBase
    {
        public StudentsController()
        {
        }

        [HttpGet]
        public List<Student> GetStudents()
        {
            return new List<Student>()
            {
                this.CreatePerson("Fabricio"),
                this.CreatePerson("Mauricio"),
                this.CreatePerson("Gabriel"),
                this.CreatePerson("Boris")
            };
        }

        [HttpPost]
        public Student CreatePerson([FromBody]string studentName)
        {
            return new Student { Name = studentName };
        }

        [HttpPut]
        public Student UpdateStudent([FromBody]Student student)
        {
            student.Name = "updated";
            return student;
        }

        [HttpDelete]
        public Student DeleteStudent([FromBody]Student student)
        {
            student.Name = "deleted";
            return student;
        }
    }
}
